import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TwinsPage } from './twins';

@NgModule({
  declarations: [
    TwinsPage,
  ],
  imports: [
    IonicPageModule.forChild(TwinsPage),
  ],
})
export class TwinsPageModule {}
