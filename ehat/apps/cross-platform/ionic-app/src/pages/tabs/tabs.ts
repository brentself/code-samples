import { Component } from '@angular/core';

import { DashboardPage } from '../dashboard/dashboard';
import { PhotosPage } from '../photos/photos';
import { ConnectionsPage } from '../connections/connections';
import { SettingsPage } from '../settings/settings';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = DashboardPage;
  tab2Root = PhotosPage;
  tab3Root = ConnectionsPage;
  tab4Root = SettingsPage;

  constructor() {

  }
}
