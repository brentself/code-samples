import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login-1');
  this.route('login');
  this.route('signup');
  this.route('recover');
  this.route('dashboard');
  this.route('twins');
  this.route('profile');
  this.route('settings');
  this.route('photos');
  this.route('connections');
  this.route('conversation');
  this.route('camera');
  this.route('share');
});

export default Router;
