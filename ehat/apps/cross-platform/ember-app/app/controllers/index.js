import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        loginUser() {
            this.transitionToRoute('dashboard');
        },
        signupUser() {
            this.transitionToRoute('signup');
        }
      }
});
