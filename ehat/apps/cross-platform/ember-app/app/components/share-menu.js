import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		ok: function() {
			$('#share-menu').removeClass('share-open');
			$('#share-menu').addClass('share-closed');
			this.sendAction('ok');
		},
		show: function() {
		  this.$('.modal').show();
		  this.sendAction('ok');
		}
	}
});
