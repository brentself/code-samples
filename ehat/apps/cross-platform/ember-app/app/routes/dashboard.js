import Ember from 'ember';

export default Ember.Route.extend({
	model() {
		return [{
			username: 'brentself',
			age: 39,
			country: 'United States',
			thumb: '/assets/images/profile-thumb.png'
		}, {
			username: 'russpeters',
			age: 43,
			country: 'Canada',
			thumb: '/assets/images/profile-thumb.png'
		}];
	}
});
