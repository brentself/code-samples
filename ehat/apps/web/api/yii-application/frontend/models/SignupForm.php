<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['facebook_username', 'trim'],
            ['facebook_username', 'email'],
            ['facebook_username', 'string', 'max' => 2000],

            ['instagram_username', 'trim'],
            ['instagram_username', 'email'],
            ['instagram_username', 'string', 'max' => 2000],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['first', 'trim'],
            ['first', 'string', 'min' => 2, 'max' => 128],

            ['last', 'trim'],
            ['last', 'string', 'min' => 2, 'max' => 128],

            ['date_of_birth', 'trim'],
            ['date_of_birth', 'required'],
            ['date_of_birth', 'date'],
            ['date_of_birth', 'string', 'max' => 255],

            ['country', 'trim'],
            ['country', 'required'],
            ['country', 'string', 'min' => 2, 'max' => 255],

            ['phone', 'trim'],
            ['phone', 'phone'],
            ['phone', 'string', 'min' => 2, 'max' => 255],

            ['status', 'trim'],
            ['status', 'bool'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            echo "failed validation";
            return null;
        } else {
            echo "passed validation";
        }
        
        $user = new User();
        $user->username = $this->username;
        if (!empty($this->facebook_username)) {
            $user->facebook_username = $this->facebook_username;
        }
        if (!empty($this->instagram_username)) {
            $user->instagram_username = $this->instagram_username;
        }
        if (!empty($this->first)) {
            $user->first = $this->first;
        }
        if (!empty($this->last)) {
            $user->last = $this->last;
        }
        if (!empty($this->phone)) {
            $user->phone = $this->phone;
        }
        if (!empty($this->status)) {
            $user->status = $this->status;
        }
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateAccessToken();
        $user->date_of_birth = $this->date_of_birth;
        $user->country = $this->country;
        
        return $user->save() ? $user : null;
    }
}
