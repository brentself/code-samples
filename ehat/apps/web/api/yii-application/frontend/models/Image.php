<?php

namespace frontend\models;

use Yii;
use frontend\models\ImageUpload;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Rekognition\RekognitionClient;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $face_id
 * @property string $url
 * @property integer $is_default
 * @property string $created
 *
 * @property User $user
 * @property ImageUpload $imageUpload
 */
class Image extends \yii\db\ActiveRecord
{
    protected $rekognition;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_default'], 'integer'],
            [['created'], 'safe'],
            [['url'], 'string', 'max' => 2000],
            [['face_id'], 'string', 'max' => 256],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'face_id' => Yii::t('app', 'Face ID'),
            'url' => Yii::t('app', 'Url'),
            'is_default' => Yii::t('app', 'Is Default'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    /**
     * @inheritdoc
     */
    // public function relations()
    // {
    //     return array(
    //         'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
    //     );
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
//        return $this->hasOne(User::className(), ['id' => 'user_id']);
		return $this->hasOne(User::findIdentityByAccessToken(), ['id' => 'user_id']);
    }
    
//    public function getUserImages()
//    {
//        return $this->find()->asArray()->where(['user_id' => Yii::$app->user->id])->all();
//    }

    /**
     * @inheritdoc
     * @return ImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImageQuery(get_called_class());
    }

    public function getImageFromS3ByKey($default_image_key)
    {
        try {
//            error_log('getting S3 image: ' . $default_image_key . ' from Bucket: ' . Yii::$app->params['s3Bucket']);
            $s3 = S3Client::factory(Yii::$app->params['s3Options']);

            $result = $s3->getObject(array(
    //              'Bucket'       => $this->bucket,
              'Bucket'       => Yii::$app->params['s3Bucket'],
              'Key'          => $default_image_key,
            ));

            // Save object to a file.
    //        $result = $s3->getObject(array(
    //            'Bucket' => Yii::$app->params['s3Bucket'],
    //            'Key'    => $default_image_key,
    //            'SaveAs' => '/tmp/' . $default_image_key
    //        ));
//            error_log('s3_result: ' . print_r($result['Body'], 1));
            if (!empty($result['Body'])) {
                return $result['Body'];
            } else {
                return false;
            }
        } catch (S3Exception $e)
        {
            error_log('s3 exc: ' . $e);
        }
    }

    /**
     * 1-100 percent
     * @property integer $accuracy
     */
    public function matchCelebrity($user_image)
    {
        if (!empty($user_image)) {
            $s3_image = $this->getImageFromS3ByKey($user_image);
            try {
              $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

              $result = $this->rekognition->RecognizeCelebrities(array(
                 'Image' => array(
                    'Bytes' => $s3_image,
//                     'S3Object' => [
//                        'Bucket' => Yii::$app->params['s3Bucket'],
//                        'Name' => $user_image,
//                        'Version' => Yii::$app->params['s3Options']['version'],
//                        'credentials' => Yii::$app->params['s3Options']['credentials'],
//                      ],
                 ),
                 'Attributes' => array('ALL')
                 )
              );
//              error_log('AWS rekCeleb: ' . print_r($result, 1));
              return $result;
            } catch (Exception $e) {
                error_log('AWS call failed: ' . print_r($result, 1));
                error_log('AWS call exc: ' . print_r($e, 1));
            }
        }
    }

    /**
     * 1-100 percent
     * @property integer $accuracy
     */
    public function match()
    {
        if (!empty($this->url)) {
            $s3_image = $this->getImageFromS3ByKey(basename($this->url));
            try {
              $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

              $result = $this->rekognition->searchFaces(array(
                 'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                 'FaceId' => $this->face_id,
                 'FaceMatchThreshold' => 70.0, //get this from user preferences
                 'MaxFaces' => 15
                 )
              );
              error_log('AWS searchFaces: ' . print_r($result, 1));
//              return $result;
            } catch (Exception $e) {
                error_log('AWS call failed: ' . print_r($result, 1));
                error_log('AWS call exc: ' . print_r($e, 1));
            }
            
            $result_celeb = $this->matchCelebrity(basename($this->url));
            error_log('AWS matchCelebrity: ' . print_r($result_celeb, 1));
            
            return json_encode($result) . json_encode($result_celeb);
        }
    }

    public function beforeDelete()
    {
        parent::beforeDelete();
        
        $image_url = $this->url;
        $image_key = basename($image_url);

        //delete S3 image
        $s3 = S3Client::factory(Yii::$app->params['s3Options']);

        $result = $s3->deleteObject(array(
            'Bucket'    => Yii::$app->params['s3Bucket'],
            'Key'       => $image_key,
        ));
    }
}
