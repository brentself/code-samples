<?php

namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
//use yii\filters\auth\QueryParamAuth;
use frontend\models\Address;

class AddressController extends ActiveController
{
    public $modelClass = 'frontend\models\Address';
    
    public function actionIndex()
    {
        if (!empty(Yii::$app->user->id)) {
            $model = new Address();
            $address = Address::findAll(['user_id' => Yii::$app->user->id]);
            $data=array_map(create_function('$m','return $m->getAttributes(array(\'id\',\'user_id\',\'address_1\',\'address_2\',\'city\',\'state\',\'zip\'));'), $address);
            return json_encode($data);
        }
    }
    
    public function actionDelete()
    {
        $request = Yii::$app->request;
        $id = $request->get('id');
        
        if (!empty($id)) {
            $address = Address::findOne(['id' => $id]);
            $response = $address->delete();
            return json_encode($response);
        }
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['index']);

        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
                // QueryParamAuth::className(),
            ],
        ];
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ($action === 'update' || $action === 'delete') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s addresses that you\'ve created.', $action));
        }

        if ($action === 'index' || $action === 'match') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot view addresses.', $action));
        }

        if ($action === 'create') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot create addresses.', $action));
        }
    }

}
