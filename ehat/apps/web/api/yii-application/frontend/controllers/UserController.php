<?php

namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;
use frontend\models\SignupForm;

class UserController extends ActiveController
{
    public $modelClass = 'frontend\models\User';

    // create a new user
    // curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST "http://api.ehat.dev/user/signup" -d '{"username": "tester", "email": "test@test.com", "password": "test123"}'

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        try {
            $model = new SignupForm();
            if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
                if ($user = $model->signup()) {
                    if (Yii::$app->getUser()->login($user)) {
                        return json_encode($user->access_token);
                    }
                } else {
                    echo "Not signed up by SignupForm";
                }
            } else {
                echo "POST not loaded by SignupForm";
            }
        } catch (Exception $e) {
            echo "POST not loaded by SignupForm exc: " . json_encode($e);
        }

        return json_encode($model);
        // return $this->render('signup', [
        //     'model' => $model,
        // ]);
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['create']);

        return $actions;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ($action === 'update' || $action === 'delete') {
            if ($model->user_id !== Yii::$app->user->id)
                throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s users that you\'ve created.', $action));
        }

        if ($action === 'index' || $action === 'match') {
            if ($model->user_id !== Yii::$app->user->id)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot view users.', $action));
        }

        // if ($action === 'create') {
        //     if ($model->user_id !== \Yii::$app->user->id)
        //         throw new \yii\web\ForbiddenHttpException(sprintf('You cannot create users.', $action));
        // }
    }
}
