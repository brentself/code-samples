<?php

namespace frontend\controllers;

use yii\rest\ActiveController;

class MessageController extends ActiveController
{
    // public function actionIndex()
    // {
    //     return $this->render('index');
    // }

    public $modelClass = 'frontend\models\Message';

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ($action === 'update' || $action === 'delete') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s messages that you\'ve created.', $action));
        }

        if ($action === 'index' || $action === 'match') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot view messages.', $action));
        }

        if ($action === 'create') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot create messages.', $action));
        }
    }

}
