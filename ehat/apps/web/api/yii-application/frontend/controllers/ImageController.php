<?php

namespace frontend\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\web\UploadedFile;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use frontend\models\Image;
use frontend\models\ImageUpload;

class ImageController extends ActiveController
{
    public $modelClass = 'frontend\models\Image';
    
    public function actionIndex()
    {
        if (!empty(Yii::$app->user->id)) {
            $model = new \frontend\models\Image();
            $images = Image::findAll(['user_id' => Yii::$app->user->id]);
            $data=array_map(create_function('$m','return $m->getAttributes(array(\'id\',\'user_id\',\'face_id\',\'url\',\'is_default\',\'created\'));'), $images);
            return json_encode($data);
        }
    }

    /**
     * Upload an image and add it to the database
     * test URL:
     * curl -i -H "Content-Type:multipart/form-data" -H "Authorization: Bearer 6hdj7__oxOK2p4ZF7ihQHsg0XN0Sfpju" -XPOST -F "imageFile=@/Users/brent/Sites/ehat/apps/web/api/yii-application/test-faces/george-w-bush/George-W-Bush.jpeg" -k "https://api.ehat.dev/image/create"
     *
     */
    public function actionCreate()
    {
        $modelImageUpload = new ImageUpload();
        
        if (Yii::$app->request->isPost) {
            $modelImageUpload->imageFile = UploadedFile::getInstanceByName('imageFile');
            
            return $modelImageUpload->upload();
        }
    }

    /**
     * Submit the current user's photos 
     * to AWS Rekognition
     *
     * Example API call:
     * curl -i -H "Accept:application/json" -H "Authorization: Bearer 6hdj7__oxOK2p4ZF7ihQHsg0XN0Sfpju" "https://api.ehat.dev/image/match"
     *
     * Example response:
     * [{\"Urls\":[\"www.imdb.com\\/name\\/nm0124133\"],\"Name\":\"George W. Bush\",\"Id\":\"2i8PO0v\",\"Face\":{\"BoundingBox\":{\"Width\":0.54411762952805,\"Height\":0.40999999642372,\"Left\":0.26176470518112,\"Top\":0.16333332657814},\"Confidence\":99.999946594238,\"Landmarks\":[{\"Type\":\"eyeLeft\",\"X\":0.4388784468174,\"Y\":0.33564421534538},{\"Type\":\"eyeRight\",\"X\":0.62337839603424,\"Y\":0.32154524326324},{\"Type\":\"nose\",\"X\":0.53598427772522,\"Y\":0.4073538184166},{\"Type\":\"mouthLeft\",\"X\":0.46305796504021,\"Y\":0.4810850918293},{\"Type\":\"mouthRight\",\"X\":0.64388829469681,\"Y\":0.46932977437973}],\"Pose\":{\"Roll\":-5.9534091949463,\"Yaw\":-4.7885756492615,\"Pitch\":2.710081577301},\"Quality\":{\"Brightness\":34.482116699219,\"Sharpness\":99.990905761719}},\"MatchConfidence\":98}]
     * 
     * return $response
	 */
    public function actionMatch()
    {
        $default_image = new \frontend\models\Image();
//        $user = new \frontend\models\User();
//        $user->load((Array)Yii::$app->user);
        $default_image = \frontend\models\Image::find()
                ->where(['user_id' => Yii::$app->user->id])
                ->orderBy('face_id DESC')
                ->one(); //$model->getUserImages();
//        error_log('typeof default image: ' . gettype($default_image));
//        $default_image = $user_images[0];
        
        if (!empty($default_image)) {
            $result = $default_image->match();
            $response = json_encode($result);
        } else {
            error_log('user images not found: ' . $default_image);
            $response = false;
        }
        
        return $response;
    }

    /**
     * Set or Get the profile (default) image
     */
    public function actionProfile()
    {
    	// get logged in user's default photo

    	// send image to match faces in collections

    	// send image to match celebrity faces
    	
    }

    public function actions()
    {
        $actions = parent::actions();

        // disable the "delete" and "create" actions
        unset($actions['index'], $actions['create']);

        return $actions;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            // 'class' => CompositeAuth::className(),
            // 'authMethods' => [
            //     HttpBearerAuth::className(),
            //     // QueryParamAuth::className(),
            // ],
            'class' => HttpBearerAuth::className(),
        ];
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ($action === 'update' || $action === 'delete') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s images that you\'ve created.', $action));
        }

        if ($action === 'index' || $action === 'match') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot view images.', $action));
        }

        if ($action === 'create') {
            if (empty(Yii::$app->user->id) === true)
                throw new \yii\web\ForbiddenHttpException(sprintf('You cannot create images.', $action));
        }
    }
}
