<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[ImageWikimedia]].
 *
 * @see ImageWikimedia
 */
class ImageWikimediaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ImageWikimedia[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImageWikimedia|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
