<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ImageWikimedia;

/**
 * ImageWikimediaSearch represents the model behind the search form about `backend\models\ImageWikimedia`.
 */
class ImageWikimediaSearch extends ImageWikimedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['face_id', 'pageid', 'title', 'categories', 'image_description', 'artist', 'credit', 'license_short_name', 'usage_terms', 'attribution', 'wikipedia_url', 'non_free', 'copyrighted', 'restrictions', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImageWikimedia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'face_id', $this->face_id])
            ->andFilterWhere(['like', 'pageid', $this->pageid])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'categories', $this->categories])
            ->andFilterWhere(['like', 'image_description', $this->image_description])
            ->andFilterWhere(['like', 'artist', $this->artist])
            ->andFilterWhere(['like', 'credit', $this->credit])
            ->andFilterWhere(['like', 'license_short_name', $this->license_short_name])
            ->andFilterWhere(['like', 'usage_terms', $this->usage_terms])
            ->andFilterWhere(['like', 'attribution', $this->attribution])
            ->andFilterWhere(['like', 'wikipedia_url', $this->wikipedia_url])
            ->andFilterWhere(['like', 'non_free', $this->non_free])
            ->andFilterWhere(['like', 'copyrighted', $this->copyrighted])
            ->andFilterWhere(['like', 'restrictions', $this->restrictions]);

        return $dataProvider;
    }
}
