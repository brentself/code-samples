<?php

namespace backend\models;

use Yii;
//use yii\imagine\Image;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Rekognition\RekognitionClient;

/**
 * This is the model class for table "image_wikimedia".
 *
 * @property integer $id
 * @property string $face_id
 * @property string $pageid
 * @property string $title
 * @property string $categories
 * @property string $image_description
 * @property string $artist
 * @property string $credit
 * @property string $license_short_name
 * @property string $usage_terms
 * @property string $attribution
 * @property string $wikipedia_url
 * @property string $s3_url
 * @property string $non_free
 * @property string $copyrighted
 * @property string $restrictions
 * @property string $created
 */
class ImageWikimedia extends \yii\db\ActiveRecord
{
    protected $rekognition;

    public $imageFile;
    
    public $allowedMimeTypes = array(
        'image/jpeg',
        'image/png'
    );
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image_wikimedia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_description'], 'string'],
            [['created'], 'safe'],
            [['face_id', 'pageid'], 'string', 'max' => 256],
            [['title', 'categories', 'artist', 'credit', 'license_short_name', 'usage_terms', 'attribution', 'wikipedia_url', 's3_url', 'non_free', 'copyrighted', 'restrictions'], 'string', 'max' => 2000],
            [['face_id'], 'unique'],
            [['pageid'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'face_id' => Yii::t('app', 'Face ID'),
            'pageid' => Yii::t('app', 'Pageid'),
            'title' => Yii::t('app', 'Title'),
            'categories' => Yii::t('app', 'Categories'),
            'image_description' => Yii::t('app', 'Image Description'),
            'artist' => Yii::t('app', 'Artist'),
            'credit' => Yii::t('app', 'Credit'),
            'license_short_name' => Yii::t('app', 'License Short Name'),
            'usage_terms' => Yii::t('app', 'Usage Terms'),
            'attribution' => Yii::t('app', 'Attribution'),
            'wikipedia_url' => Yii::t('app', 'Wikipedia Url'),
            's3_url' => Yii::t('app', 'S3 Url (auto generated)'),
            'non_free' => Yii::t('app', 'Non Free'),
            'copyrighted' => Yii::t('app', 'Copyrighted'),
            'restrictions' => Yii::t('app', 'Restrictions'),
            'created' => Yii::t('app', 'Created'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function updateImage()
    {
        $face_detected = $this->detectFaces();
        if ($face_detected === true) {
            // save to generate image ID
            $this->save();
//            echo "imageWmModel saved\n";
//            echo print_r($this, 1);
            
            $s3_response = $this->saveToS3();
            if (!empty($s3_response['ObjectURL'])) {
                $this->s3_url = $s3_response['ObjectURL'];
                echo "s3_url: " . $this->s3_url . "\n";
                //is this necessary? is the object returned by the putObject?
    //            $s3_image = $this->getImageFromS3ByKey(basename($this->s3_url));

    //            if ($s3_image !== false) {
    //                $this->saveToRekognition($s3_image);
                $rek_result = $this->saveToRekognition('');
    //            }
                if ($rek_result === true) {
                    // save the updated url from S3 and face_id from Rek
                    $this->update();
                } else {
                    $this->delete();
                }
            }
        } else {
            error_log("face not detected in image: " . $this->imageFile);
        }
        
        return true;
    }

    /**
     * 
     */
    public function detectFaces()
    {
        try {
            #Get local image
            if (file_exists($this->imageFile) && filesize($this->imageFile) < 5242880) {
                if (in_array(mime_content_type($this->imageFile), $this->allowedMimeTypes)) {
                    $image_dimensions = getimagesize($this->imageFile);
                    if ($image_dimensions[0] > 80 && $image_dimensions[1] > 80) {
                        $fp_image = fopen($this->imageFile, 'r');
                        $image = fread($fp_image, filesize($this->imageFile));
                        fclose($fp_image);

        //                $imagine = new yii\imagine\Image();
        //                $image_file = $imagine->open($this->imageFile);
        //                $image_file->

                        echo "file handle opened mem used: " . memory_get_usage() . "\n";
                        $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);
                        echo "rek client created mem used: " . memory_get_usage() . "\n";
                        # Call DetectFaces to verify image contains a single face
                        $result = $this->rekognition->DetectFaces(array(
                           'Image' => array(
                              'Bytes' => $image,
                           ),
                           'Attributes' => array('ALL')
                           )
                        );

                        // free memory
                        unset($fp_image);
                        unset($image);
                        unset($result);

                        return true;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Aws\Rekognition\Exception\RekognitionException $e) {
            error_log('AWS DetectFaces call failed, exc: ' . print_r($e, 1));
            return false;
        } catch (Imagine\Exception\InvalidArgumentException $e) {
            error_log('AWS DetectFaces failed, Imagine InvalidArgumentException: ' . print_r($e, 1));
            return false;
        } catch (Imagine\Exception\RuntimeException $e) {
            error_log('AWS DetectFaces failed, Imagine RuntimeException: ' . print_r($e, 1));
            return false;
        }
    }
    
    public function saveToS3()
    {
        $keyname = basename($this->imageFile);
        echo "ImgWm saveToS3 keyname: " . $keyname . ", tmp file: " . $this->imageFile . "\n";

        // Save image to S3 storage bucket
        $s3 = S3Client::factory(Yii::$app->params['s3Options']);

        $s3_result = $s3->putObject(array(
          'Bucket'       => Yii::$app->params['s3BucketWikimedia'],
          'Key'          => $keyname,
          'SourceFile'   => $this->imageFile,
           'ContentType'  => mime_content_type($this->imageFile),
           'ACL'          => 'public-read',
          // 'StorageClass' => 'REDUCED_REDUNDANCY',
          'Metadata'     => array(    
              'x-amz-meta-image_id' => $this->id
          )
        ));
        
//        error_log("s3 result: " . print_r($s3_result, 1));

        return $s3_result;
    }
    
    public function saveToRekognition($s3_image)
    {
        if (!empty($this->id)) {
            try {
                // Save image data in a Rekognition Collection
                $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

                $keyname = basename($this->imageFile);

                $fp_image = fopen($this->imageFile, 'r');
                $image = fread($fp_image, filesize($this->imageFile));
                fclose($fp_image);

                $result = $this->rekognition->indexFaces(array(
                    'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                    'DetectionAttributes' => ['ALL'],
                    'ExternalImageId' => (string)$this->id,
                    'Image' => array(
                       'Bytes' => $image,
                    )
    //                'Image' => [
    //                    'S3Object' => [
    //                        'Bucket' => Yii::$app->params['s3BucketWikimedia'],
    //                        'Name' => $keyname,
    //                    ],
    //                ]
                   )
                );

        //            error_log('indexFaces result: ' . print_r($result, 1));

                if (!empty($result['FaceRecords'][0]['Face']['FaceId'])) {
                    $face_id = $result['FaceRecords'][0]['Face']['FaceId'];
                    $this->face_id = $face_id;

    //                if (!empty($s3_response)) {
    //                    $this->url = $s3_response;
    //                }
                    return true;
                } else {
                    echo "index faces result: " . print_r($result, 1);
                    return false;
                }
            } catch (Exception $e) {
                error_log("error indexing and saving image to rek: " . print_r($e, 1));
                return false;
            }
        }
    }

    /**
     * @inheritdoc
     * @return ImageWikimediaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImageWikimediaQuery(get_called_class());
    }
    
    /**
     * 1-100 percent
     * @property integer $accuracy
     */
    public function match()
    {
        $all_matches = array();
        
        if (!empty($this->s3_url) && !empty($this->face_id)) {
//            $s3_image = $this->getImageFromS3ByKey(basename($this->s3_url));
            try {
              $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

              $result = $this->rekognition->searchFaces(array(
                 'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                 'FaceId' => $this->face_id,
                 'FaceMatchThreshold' => (!empty(Yii::$app->request->post('thresh'))) ? floatval(Yii::$app->request->post('thresh') / 2) : Yii::$app->params['defaultMatchThreshWiki'], //15.0, //get this from user preferences
                 'MaxFaces' => 50
                 )
              );
//              error_log('AWS searchFaces: ' . print_r($result, 1));
              $all_matches['ehat'] = $result;
            } catch (Exception $e) {
                error_log('AWS searchFaces call failed: ' . print_r($result, 1));
                error_log('AWS searchFaces call exc: ' . print_r($e, 1));
            }
            
            return $all_matches;
        } else {
            error_log('s3_url and face_id empty');
        }
    }
    
    public function s3ImageExists()
    {
        $url_headers = get_headers($this->s3_url);
        
        if ($url_headers[0] === 'HTTP/1.1 200 OK') {
            return true;
        } else {
            return false;
        }
    }
    
    public function saveImageUrlToS3()
    {
        $tmp_file = "/tmp/" . basename($this->s3_url);
        if ($this->getRemoteImage($this->wikipedia_url, $tmp_file) === true) {
            $this->imageFile = $tmp_file;
            $s3_result = $this->saveToS3();
            
            if (filter_var($s3_result['ObjectURL'], FILTER_VALIDATE_URL) !== false) {
                return $s3_result['ObjectURL'];
            } else {
                return false;
            }
        }
    }
    
    public function getRemoteImage($url, $tmp_dest) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        $raw = curl_exec($ch);
        curl_close ($ch);
        
        echo "retrieved remote image mem used: " . memory_get_usage() . "\n";
        
        if (file_exists($tmp_dest)) {
            unlink($tmp_dest);
        }
        
        $fp = fopen($tmp_dest,'x');
        fwrite($fp, $raw);
        fclose($fp);
        
        unset($ch);
        unset($raw);
        echo "remote image ref unset mem used: " . memory_get_usage() . "\n";
        if (file_exists($tmp_dest)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function beforeDelete()
    {
        parent::beforeDelete();
        
        //delete S3 image
        $image_url = $this->s3_url;
        if (!empty($image_url)) {
            if ($image_url !== "NULL") {
                $image_key = basename($image_url);

                $s3 = S3Client::factory(Yii::$app->params['s3Options']);
                $result = $s3->deleteObject(array(
                    'Bucket'    => Yii::$app->params['s3BucketWikimedia'],
                    'Key'       => $image_key,
                ));
            }
        }
        
        //delete Rekognition face data
        $face_id = $this->face_id;
        if (!empty($face_id)) {
            if ($face_id !== "NULL") {
                try {
                    $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

                    $resultRek = $this->rekognition->deleteFaces(array(
                        'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                        'FaceIds' => [(string)$face_id],
                       )
                    );
                } catch (Aws\Rekognition\Exception\RekognitionException $e) {
                    error_log("Rekognition exception: " . print_r($e, 1));
                }
            }
        }
        
        return true;
    }
}
