<?php

namespace backend\models;

use Yii;
use backend\models\ImageUpload;
use yii\web\UploadedFile;
// use yii\imagine\Image;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Rekognition\RekognitionClient;

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $face_id
 * @property string $url
 * @property integer $is_default
 * @property string $created
 *
 * @property User $user
 * @property ImageUpload $imageUpload
 */
class Image extends \yii\db\ActiveRecord
{
    protected $rekognition;

    public $imageFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_default'], 'integer'],
            [['created'], 'safe'],
//            [
//                ['url'], 
//                'image', 
//                'skipOnEmpty' => false, 
//                'minWidth' => 80, 
//                'minHeight' => 80, 
//                'maxSize' => 5 * 1024 * 1024, 
//                'extensions' => 'png, jpg, jpeg'
//            ],
            [['url'], 'string', 'max' => 2000],
            [['face_id'], 'string', 'max' => 256],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'face_id' => Yii::t('app', 'Face ID'),
            'url' => Yii::t('app', 'Image'),
            'is_default' => Yii::t('app', 'Is Default'),
            'created' => Yii::t('app', 'Created'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function updateImage()
    {
        // save to generate image ID
        $this->save();
        $s3_response = $this->saveToS3();
        $this->url = $s3_response['ObjectURL'];
        //is this necessary? is the object returned by the putObject?
        $s3_image = $this->getImageFromS3ByKey(basename($this->url));
        $this->saveToRekognition($s3_image);
        // save the updated url from S3 and face_id from Rek
        $this->update();
        return true;
    }
    
    public function saveToS3()
    {
        $keyname = $this->id . '_' . basename($this->imageFile->name);

        // Save image to S3 storage bucket
        $s3 = S3Client::factory(Yii::$app->params['s3Options']);

        $s3_result = $s3->putObject(array(
          'Bucket'       => Yii::$app->params['s3Bucket'],
          'Key'          => $keyname,
          'SourceFile'   => $this->imageFile->tempName,
           'ContentType'  => $this->imageFile->type,
           'ACL'          => 'public-read',
          // 'StorageClass' => 'REDUCED_REDUNDANCY',
          'Metadata'     => array(    
              'x-amz-meta-image_id' => $this->id,
              'x-amz-meta-user_id' => $this->user_id
          )
        ));
        
        error_log("s3 result: " . print_r($s3_result, 1));

        return $s3_result;
    }
    
    public function saveToRekognition($s3_image)
    {
        // Save image data in a Rekognition Collection
        $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

        $result = $this->rekognition->indexFaces(array(
            'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
            'DetectionAttributes' => ['ALL'],
            'ExternalImageId' => (string)$this->id,
            'Image' => array(
               'Bytes' => $s3_image,
            )
//                'Image' => [
//                    'S3Object' => [
//                        'Bucket' => Yii::$app->params['s3Bucket'],
//                        'Name' => $keyname,
//                    ],
//                ]
           )
        );

//            error_log('indexFaces result: ' . print_r($result, 1));

        $face_id = $result['FaceRecords'][0]['Face']['FaceId'];

        if (!empty($face_id)) {
            $this->face_id = $face_id;
        }

        if (!empty($s3_response)) {
            $this->url = $s3_response;
        }
    }

    /**
     * @inheritdoc
     */
    // public function relations()
    // {
    //     return array(
    //         'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
    //     );
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImageQuery(get_called_class());
    }

    public function getImageFromS3ByKey($default_image_key)
    {
        try {
            $s3 = S3Client::factory(Yii::$app->params['s3Options']);

            $result = $s3->getObject(array(
              'Bucket'       => Yii::$app->params['s3Bucket'],
              'Key'          => $default_image_key,
            ));

            // Save object to a file.
    //        $result = $s3->getObject(array(
    //            'Bucket' => Yii::$app->params['s3Bucket'],
    //            'Key'    => $default_image_key,
    //            'SaveAs' => '/tmp/' . $default_image_key
    //        ));
//            error_log('s3_result: ' . print_r($result['Body'], 1));
            if (!empty($result['Body'])) {
                return $result['Body'];
            } else {
                return false;
            }
        } catch (S3Exception $e)
        {
            error_log('s3 exc: ' . $e);
        }
    }

    /**
     * 1-100 percent
     * @property integer $accuracy
     */
//    public function matchCelebrity($user_image)
//    {
//        if (!empty($user_image)) {
//            $s3_image = $this->getImageFromS3ByKey($user_image);
//            try {
//              $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);
//
//              $result = $this->rekognition->RecognizeCelebrities(array(
//                 'Image' => array(
//                    'Bytes' => $s3_image,
////                     'S3Object' => [
////                        'Bucket' => Yii::$app->params['s3Bucket'],
////                        'Name' => $user_image,
////                        'Version' => Yii::$app->params['s3Options']['version'],
////                        'credentials' => Yii::$app->params['s3Options']['credentials'],
////                      ],
//                 ),
//                 'Attributes' => array('ALL')
//                 )
//              );
////              error_log('AWS rekCeleb: ' . print_r($result, 1));
//              return $result;
//            } catch (Exception $e) {
//                error_log('AWS call failed: ' . print_r($result, 1));
//                error_log('AWS call exc: ' . print_r($e, 1));
//            }
//        }
//    }

    /**
     * 1-100 percent
     * @property integer $accuracy
     */
    public function match()
    {
        $all_matches = array();
        
        if (!empty($this->url)) {
            $s3_image = $this->getImageFromS3ByKey(basename($this->url));
            try {
              $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

              $result = $this->rekognition->searchFaces(array(
                 'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                 'FaceId' => $this->face_id,
                 'FaceMatchThreshold' => (!empty(Yii::$app->request->post('thresh'))) ? floatval(Yii::$app->request->post('thresh')) : Yii::$app->params['defaultMatchThreshWiki'], //15.0, //get this from user preferences
                 'MaxFaces' => 15
                 )
              );
              error_log('AWS searchFaces: ' . print_r($result, 1));
              $all_matches['ehat'] = $result;
            } catch (Exception $e) {
                error_log('AWS searchFaces call failed: ' . print_r($result, 1));
                error_log('AWS searchFaces call exc: ' . print_r($e, 1));
            }
            
//            try {
//                // no percentage available?
//                // http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-rekognition-2016-06-27.html#recognizecelebrities
//                // perhaps send a blk & wht version, blur as well?
//                $result_celeb = $this->matchCelebrity(basename($this->url));
//                error_log('AWS matchCelebrity: ' . print_r($result_celeb, 1));
//                $all_matches['celebrities'] = $result_celeb;
//            } catch (Exception $e) {
//                error_log('AWS matchCelebrity call failed: ' . print_r($result, 1));
//                error_log('AWS matchCelebrity call exc: ' . print_r($e, 1));
//            }
            
//            return json_encode($result) . json_encode($result_celeb);
            return $all_matches;
        }
    }

    public function beforeDelete()
    {
        parent::beforeDelete();
        
        //delete S3 image
        $image_url = $this->url;
        if (!empty($image_url)) {
            if ($image_url !== "NULL") {
                $image_key = basename($image_url);

                $s3 = S3Client::factory(Yii::$app->params['s3Options']);
                $result = $s3->deleteObject(array(
                    'Bucket'    => Yii::$app->params['s3Bucket'],
                    'Key'       => $image_key,
                ));
            }
        }
        
        //delete Rekognition face data
        $face_id = $this->face_id;
        if (!empty($face_id)) {
            if ($face_id !== "NULL") {
                $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

                $resultRek = $this->rekognition->deleteFaces(array(
                    'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                    'FaceIds' => [(string)$face_id],
                   )
                );
            }
        }
        
        return true;
    }
}
