<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Aws\S3\S3Client;
use Aws\Rekognition\RekognitionClient;

require '/usr/share/nginx/html/vendor/autoload.php';

/**
 * This is the model class for table "image".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $url
 * @property integer $is_default
 * @property string $created
 *
 * @property User $user
 */
class ImageDownload extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageBaseUrl = '/usr/share/nginx/html/test-faces/';

    public $imageFile;

    public $imageSizeMax = 5242880; // Amazon Rek limit
    
    private $rekognition;

    public function download()
    {
        if ($this->validate()) {
            $model = new \frontend\models\Image();
            $model->user_id = \Yii::$app->user->id;
            $model->is_default = 0;
            $model->save();
            
            $keyname = $model->id . '_' . $this->imageFile->baseName . '.' . $this->imageFile->extension;

            // Save image to S3 storage bucket
            $s3 = S3Client::factory(Yii::$app->params['s3Options']);
            
            $s3_result = $s3->putObject(array(
              'Bucket'       => Yii::$app->params['s3Bucket'],
              'Key'          => $keyname,
              'SourceFile'   => $this->imageFile->tempName,
              // 'ContentType'  => 'text/plain',
              // 'ACL'          => 'public-read',
              // 'StorageClass' => 'REDUCED_REDUNDANCY',
              'Metadata'     => array(    
                  'x-amz-meta-image_id' => $model->id,
                  'x-amz-meta-user_id' => Yii::$app->user->id
              )
            ));
            
            $s3_response = $s3_result['ObjectURL'];
            
            //is this necessary? is the object returned by the putObject?
            $s3_image = $model->getImageFromS3ByKey(basename($s3_response));

            // Save image data in a Rekognition Collection
            $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

            $result = $this->rekognition->indexFaces(array(
                'CollectionId' => Yii::$app->params['rekognitionCollectionId'],
                'DetectionAttributes' => ['ALL'],
                'ExternalImageId' => (string)$model->id,
                'Image' => array(
                   'Bytes' => $s3_image,
                )
//                'Image' => [
//                    'S3Object' => [
//                        'Bucket' => Yii::$app->params['s3Bucket'],
//                        'Name' => $keyname,
//                    ],
//                ]
               )
            );
            
//            error_log('indexFaces result: ' . print_r($result, 1));
            
            $face_id = $result['FaceRecords'][0]['Face']['FaceId'];
            
            if (!empty($face_id)) {
                $model->face_id = $face_id;
            }

            if (!empty($s3_response)) {
                $model->url = $s3_response;
                $model->save();
                
                return $s3_response;
            } else {
              return false;
            }
        } else {
            error_log("img not validated");
            return false;
        }
    }

    /**
     * 
     */
    public function detectFaces()
    {
        try {
            #Get local image
            $image_loc = $this->imageFile->tempName;

            // if (filesize($image_loc) > $this->imageSizeMax && filesize($image_loc) < (2 * $this->imageSizeMax)) {
            //     //reduce image size by ratio
            //     // filesize / $this->imageSizeMax
            //     $imagine = new \Imagine\Imagick\Imagine();
            //     $image_resized = $imagine->open($image_loc);
            //     $size = $image_resized->getSize();
            //     $resize_ratio = filesize($image_loc) / $this->imageSizeMax;
            //     $image_resized->resize(new \Imagine\Image\Box($size->getWidth() / $resize_ratio, $size->getHeight() / $resize_ratio));
            // }
            
            $fp_image = fopen($image_loc, 'r');
            $image = fread($fp_image, filesize($image_loc));
            fclose($fp_image);

            $this->rekognition = new RekognitionClient(Yii::$app->params['rekognitionOptions']);

            # Call DetectFaces to verify image contains a single face
            $result = $this->rekognition->DetectFaces(array(
               'Image' => array(
                  'Bytes' => $image,
               ),
               'Attributes' => array('ALL')
               )
            );

            return true;
        } catch (Exception $e) {
            error_log('AWS DetectFaces call failed, result: ' . print_r($result, 1));
            error_log('AWS DetectFaces call failed, exc: ' . print_r($e, 1));
            return false;
        }
    }

    /**
     * Validation rules
     *
     * Amazon image reqs:
     * http://docs.aws.amazon.com/rekognition/latest/dg/limits.html
     *
     * S3 max file size: 15 MB
     * API max raw byte size: 5 MB
     * 1 million faces per collection max
     */
    public function rules()
    {
        return [
            [
                ['imageFile'], 
                'image', 
                'skipOnEmpty' => false, 
                'minWidth' => 80, 
                'minHeight' => 80, 
                'maxSize' => 5 * 1024 * 1024, 
                'extensions' => 'png, PNG, jpg, jpeg, JPG, JPEG'
            ],
        ];
    }

    /**
     * Event after validation rules
     */
    public function afterValidate()
    {
        // error_log("afterValidation");
        if ($this->detectFaces() === true) {
            // error_log("truly valid");
            return true;
        } else {
            // error_log("falsely valid");
            return false;
        }
    }
}
