<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Image;
use backend\models\RecognizeCelebritiesDataProvider;

/**
 * ImageSearch represents the model behind the search form about `backend\models\Image`.
 */
class CelebrityMatch extends Image
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'is_default'], 'integer'],
            [['url', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Image::find();

        // get matching FaceIds from Rek
        $image = Image::find()
            ->where(['id' => $params['id']])
            ->one();

        $dataProvider = new ActiveDataProvider([
//        $dataProvider = new ImageMatchDataProvider([
            'query' => $query,
        ]);
        
        $all_matches = $image->match();

        if (empty($image->face_id) === true || (empty($all_matches['ehat']) === true && empty($all_matches['celebrities']) === true)) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($all_matches['ehat']['FaceMatches'])) {
            foreach ($all_matches['ehat']['FaceMatches'] as $k => $v) {
                $face_id = $v['Face']['FaceId'];
                if (!empty($face_id)) {
                    $query->orFilterWhere(['=', 'face_id', $v['Face']['FaceId']]);
                }
            }

            $query->andFilterWhere(['<>', 'id', $image->id]);
        } else {
            $query->where('0=1');
        }
        
        $q = $query->createCommand()->getRawSql();
        error_log('q: ' . $q);
        
//        $dataProvider->celebrities = $all_matches['celebrities'];
        return $dataProvider;
    }
}
