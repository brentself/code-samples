<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Image;
use backend\models\ImageWikimedia;

/**
 * ImageSearch represents the model behind the search form about `backend\models\Image`.
 */
class ImageWikimediaMatch extends ImageWikimedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['s3_url', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImageWikimedia::find();

        // get matching FaceIds from Rek
        $image = Image::find()
            ->where(['id' => $params['id']])
            ->one();

        $dataProvider = new ActiveDataProvider([
//        $dataProvider = new ImageMatchDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => -1,
            ],
        ]);
        
        $image_wm = new ImageWikimedia();
        $image_wm->s3_url = $image->url;
        $image_wm->face_id = $image->face_id;
        $all_matches = $image_wm->match();

        if (empty($image->face_id) === true || empty($all_matches['ehat']) === true) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }
        
        if (!empty($all_matches['ehat']['FaceMatches'])) {
            foreach ($all_matches['ehat']['FaceMatches'] as $k => $v) {
                $face_id = $v['Face']['FaceId'];
                if (!empty($face_id)) {
                    $query->orFilterWhere(['=', 'face_id', $v['Face']['FaceId']]);
                }
            }

            $query->andFilterWhere(['<>', 'id', $image->id]);
        } else {
            $query->where('0=1');
        }
        
//        $q = $query->createCommand()->getRawSql();
//        error_log('q: ' . $q);
        
//        $dataProvider->celebrities = $all_matches['celebrities'];
        return $dataProvider;
    }
}
