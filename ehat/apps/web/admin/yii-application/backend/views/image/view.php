<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\jui\SliderInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Image */
/* @var $searchModel backend\models\ImageMatch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Images'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
//            'url:image',
            [
                'attribute'=>'url',
                'value'=>$model->url,
                'format' => ['image',['width'=>'240']],
            ],
            'is_default',
            'created',
        ],
    ]) ?>
    
    <h2><?= Html::encode('EHAT Twins') ?></h2>
    <?php Pjax::begin(); ?>
    <?= Html::beginForm(
        [
            'image/view', 
            'id' => Yii::$app->request->get('id')
        ], 
        'post', 
        [
            'data-pjax' => '', 
            'class' => 'form-inline'
        ]); ?>
    <?= SliderInput::widget([
        'name' => 'thresh',
        'clientEvents' => [
            'change' => 'function() { $(this).closest("form").submit(); }',
        ],
        'clientOptions' => [
            'class' => 'update-twins',
            'min' => 0.5,
            'max' => 99.5,
            'step' => 0.5,
        ],
        'value' => (!empty(Yii::$app->request->post('thresh'))) ? Yii::$app->request->post('thresh') : Yii::$app->params['defaultMatchThreshWiki'],
    ]); ?>
    <?= Html::endForm() ?>
    <?= Html::tag('div', '', array(
        'id' => 'celeb-twin-list-loading'
    )) ?>
    <?php
//    echo GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            [
//                'attribute'=>'user',
//                'label'=>'Username',
//                'format'=>'html',
//                'content' => function($data){
//                    $username = $data->user['username'];
//                    return Html::encode($username);
//                }
//            ],
//            [
//                'attribute'=>'url',
//                'label'=>'Image',
//                'format'=>'html',
//                'content' => function($data){
//                    $url = $data->url;
//                    return Html::img($url, ['alt'=>'yii','width'=>'120']);
//                }
//            ],
//            'is_default',
//            'created',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]);
    ?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_ehat-twin',
        'options' => array(
            'class' => 'ehat-twin-list-view',
        ),
    ]); ?>
    
    <br>
    
    <h2><?= Html::encode('Celebrity Twins') ?></h2>
    
    <?= ListView::widget([
        'dataProvider' => $dataProviderCeleb,
        'itemView' => '_celebrity-twin',
        'options' => array(
            'class' => 'celeb-twin-list-view',
        ),
    ]); ?>
    
    <br>
    
    <?php Pjax::end(); ?>
</div>
