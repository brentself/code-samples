<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="celebrity-twin">
    <?= Html::a(
            Html::img(
                $model->s3_url, 
                array(
                    "data-id" => Html::encode($model->id)
                )
            ), 
        $model->wikipedia_url,
        array(
            'target' => '_blank'
        )
    ) ?>
    <?= Html::tag(
            'div', 
            Html::a(
                ucwords(HtmlPurifier::process($model->image_description)), 
                $model->wikipedia_url,
                array(
                    'target' => '_blank'
                )
            ),
            array(
                'class' => 'celebrity-image-desc',
            )
        ) ?>
</div>