<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;
?>
<div class="celebrity-twin">
    <?= Html::a(
            Html::img(
                $model->url, 
                array(
                    "data-id" => Html::encode($model->id)
                )
            ), 
        Url::to(['user/view', 'id' => $model->user_id]),
        array(
            'target' => '_blank'
        )
    ) ?>
    <?= Html::tag(
            'div', 
            Html::a(
                ucwords(HtmlPurifier::process($model->user->username)), 
                Url::to(['user/view', 'id' => $model->user_id]),
                array(
                    'target' => '_blank'
                )
            ),
            array(
                'class' => 'celebrity-image-desc',
            )
        ) ?>
</div>