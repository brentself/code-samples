<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ImageWikimedia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-wikimedia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wikipedia_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 's3_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'face_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pageid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categories')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'artist')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'credit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'license_short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usage_terms')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'attribution')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'non_free')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'copyrighted')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'restrictions')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
