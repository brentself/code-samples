<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ImageWikimediaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-wikimedia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'face_id') ?>

    <?= $form->field($model, 'pageid') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'categories') ?>

    <?php // echo $form->field($model, 'image_description') ?>

    <?php // echo $form->field($model, 'artist') ?>

    <?php // echo $form->field($model, 'credit') ?>

    <?php // echo $form->field($model, 'license_short_name') ?>

    <?php // echo $form->field($model, 'usage_terms') ?>

    <?php // echo $form->field($model, 'attribution') ?>

    <?php // echo $form->field($model, 'wikipedia_url') ?>

    <?php // echo $form->field($model, 'non_free') ?>

    <?php // echo $form->field($model, 'copyrighted') ?>

    <?php // echo $form->field($model, 'restrictions') ?>

    <?php // echo $form->field($model, 'created') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
