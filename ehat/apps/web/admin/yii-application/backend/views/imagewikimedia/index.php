<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ImageWikimediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Image Wikimedia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-wikimedia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Image Wikimedia'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

            ['class' => 'yii\grid\ActionColumn'],

            'id',
            [
                'attribute'=>'s3_url',
                'label'=>'Image',
                'format'=>'html',
                'content' => function($data){
                    $url = $data->s3_url;
                    return Html::img($url, ['alt'=>'yii','width'=>'120']);
                }
            ],
            'face_id',
            'pageid',
            'title',
            'categories',
             'image_description:ntext',
             'artist',
             'credit',
             'license_short_name',
             'usage_terms',
             'attribution',
             'wikipedia_url:url',
             'non_free',
             'copyrighted',
             'restrictions',
             'created',
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
