<?php

/* @var $this yii\web\View */

$this->title = 'EHAT Admin';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>EHAT</h1>

        <p class="lead">everyone has a twin</p>

        <p><a class="btn btn-lg btn-success" href="http://www.ehat.com">Get Started!</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Search For Twins</h2>

                <p>EHAT (Everyone Has A Twin) will help you get started. Who knows ... you may even find your long lost twin. EHAT plans to donate 10% of all proceeds to organizations focused solely on social responsibility.</p>

                <p><a class="btn btn-default" href="http://www.ehat.com/twins/">Read More &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Send Messages</h2>

                <p>Think you look alike? Maybe you have some other things in common. Strike up a conversation to find out!</p>

                <p><a class="btn btn-default" href="http://www.ehat.com/messaging/">Read More &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Connect and Share</h2>

                <p>We live in an economically, socially, and politically interconnected world. Start building your global network of contacts. </p>

                <p><a class="btn btn-default" href="http://www.ehat.com/social/">Read More &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
