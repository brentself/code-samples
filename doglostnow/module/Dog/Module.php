<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Dog;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Dog\Model\Dog;
use Dog\Model\DogTable;
use Dog\Model\DogLost;
use Dog\Model\DogLostTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
//        $this->initAcl($e);
		$eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
        );
    }
	
	public function getServiceConfig()
	{
		return array(
			'factories' => array(
				'Dog\Model\DogTable' =>  function($sm) {
					$tableGateway = $sm->get('DogTableGateway');
					$table = new DogTable($tableGateway);
					return $table;
				},
				'DogTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new Dog());
					return new TableGateway('dog', $dbAdapter, null, $resultSetPrototype);
				},
// 				},
				'Dog\Model\DogLostTable' =>  function($sm) {
					$tableGateway = $sm->get('DogLostTableGateway');
					$table = new DogLostTable($tableGateway);
					return $table;
				},
				'DogLostTableGateway' => function ($sm) {
					$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
					$resultSetPrototype = new ResultSet();
					$resultSetPrototype->setArrayObjectPrototype(new DogLost());
					return new TableGateway('dog_lost', $dbAdapter, null, $resultSetPrototype);
				},
			),
		);
	}
}
