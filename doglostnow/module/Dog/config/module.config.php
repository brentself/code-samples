<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'dog' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/dog[/][:action][/:id]',
					'constraints'	=> array(
						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'		=> '[0-9]+'
					),
                    'defaults' => array(
                        'controller' => 'Dog\Controller\Dog',
                        'action'     => 'index',
                    ),
                ),
            ),
            'add-dog' => array(
            		'type' => 'segment',
            		'options' => array(
            				'route'    => '/dog[/][:action][/:id]',
            				'constraints'	=> array(
            						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
            						'id'		=> '[0-9]+'
            				),
            				'defaults' => array(
            						'controller' => 'Dog\Controller\Dog',
            						'action'     => 'add',
            				),
            		),
            ),
            'add-lost-dog' => array(
            		'type' => 'segment',
            		'options' => array(
            				'route'    => '/dog[/][:action][/:id]',
            				'constraints'	=> array(
            						'action'	=> '[a-zA-Z][a-zA-Z0-9_-]*',
            						'id'		=> '[0-9]+'
            				),
            				'defaults' => array(
            						'controller' => 'Dog\Controller\Dog',
            						'action'     => 'lost',
            				),
            		),
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
			'Dog\Controller\Dog' => 'Dog\Controller\DogController'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'dog'	=> __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
