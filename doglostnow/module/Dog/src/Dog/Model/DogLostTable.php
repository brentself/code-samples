<?php
namespace Dog\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * Description of DogLost
 *
 * @author brent
 */
class DogLostTable {
	protected $tableGateway;
	
	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}
	
	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	
	public function getDogLost($id)
	{
		$id = (int)$id;
		$rowSet = $this->tableGateway->select(array('id' => $id));
		$row = $rowSet->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}
	
	public function saveDogLost(DogLost $dogLost)
	{
		$data = array(
	        'dog_id' => $dogLost->dog_id,
	        'address_id' => $dogLost->address_id,
	        'lost_status' => $dogLost->lost_status,
			'description'  => $dogLost->description,
	        'lost_at' => $dogLost->lost_at,
	        'created_at' => date('Y-m-d H:i:s'),
	        'updated_at' => date('Y-m-d H:i:s'),
		);
		error_log(print_r($data, true));

		$id = (int) $dogLost->id;
		if ($id == 0) {
			$this->tableGateway->insert($data);
		} else {
			if ($this->getDogLost($id)) {
				$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('DogLost id does not exist');
			}
		}
	}
	
	public function deleteDogLost($id)
	{
		$this->tableGateway->delete(array('id' => (int) $id));
	}
}
