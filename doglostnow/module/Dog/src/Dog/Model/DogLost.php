<?php
namespace Dog\Model;
/**
 * Description of DogLost
 *
 * @author brent
 */

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DogLost implements InputFilterAwareInterface
{
    public $id;
    public $dog_id;
    public $address_id;
	public $lost_status;
	public $description;
	public $lost_at;
	public $created_at;
	public $updated_at;
	protected $inputFilter;
	
	public function exchangeArray($data)
	{
// 	    $this->id = (!empty($data['dog_lost']['id'])) ? $data['dog_lost']['id'] : null;
	    $this->dog_id = (!empty($data['dog_id'])) ? $data['dog_id'] : null;
		$this->address_id = (!empty($data['address_id'])) ? $data['address_id'] : null;
		$this->lost_status = 1;
// 		$this->name = (!empty($data['name'])) ? $data['name'] : null;
		$this->description = (!empty($data['description'])) ? $data['description'] : null;
		$this->lost_at = (!empty($data['lost_at'])) ? $data['lost_at'] : null;
		$this->created_at = date('Y-m-d H:i:s');
		$this->updated_at = date('Y-m-d H:i:s');
	}
	
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

// 			$inputFilter->add(array(
// 				'name'     => 'id',
// 				'required' => true,
// 				'filters'  => array(
// 					array('name' => 'Int'),
// 				),
// 			));

			$inputFilter->add(array(
				'name'     => 'dog_id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

			$inputFilter->add(array(
				'name'     => 'address_id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			));

// 			$inputFilter->add(array(
// 				'name'     => 'lost_status_id',
// 				'required' => true,
// 				'filters'  => array(
// 					array('name' => 'Int'),
// 				),
// 			));

			$inputFilter->add(array(
				'name'     => 'description',
				'required' => false,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 300,
						),
					),
				),
			));

			$inputFilter->add(array(
				'name'     => 'lost_at',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 64,
						),
					),
				),
			));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}
