<?php
namespace Dog\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

/**
 * Description of Dog
 *
 * @author brent
 */
class DogTable {
	protected $tableGateway;
	
	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}
	
	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	
	public function getDog($id)
	{
		$id = (int)$id;
		$rowSet = $this->tableGateway->select(array('id' => $id));
		$row = $rowSet->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}
	
	public function getDogsByDogId($id)
	{
		$id = (int)$id;
		$rowSet = $this->tableGateway->select(array('id' => $id));
		$row = $rowSet->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}
	
	public function getDogsByUserId($id)
	{
		$id = (int)$id;
		$where = new Where();
		$where->equalTo('user_dog.user_id', $id);
		
		$sqlSelect = $this->tableGateway->getSql()->select()->where($where);
		$sqlSelect->join('user_dog', 'dog.id = user_dog.dog_id', array('*'), 'left');
		$resultSet = $this->tableGateway->selectWith($sqlSelect);
		
		$resultSetArray = array();
		if (!$resultSet) {
			throw new \Exception("Could not find row $id");
		} else {
			foreach ($resultSet as $result) {
				$resultSetArray[] = $result;
			}
		}
		return $resultSetArray;
	}
	
	public function saveDog(Dog $dog)
	{
	    $data = array(
			'name' => $dog->name,
			'description'  => $dog->description,
		);
        
		$id = (int) $dog->id;
		if ($id == 0) {
			$this->tableGateway->insert($data);
			$id = $this->tableGateway->lastInsertValue;
		} else {
			if ($this->getDog($id)) {
				$this->tableGateway->update($data, array('id' => $id));
			} else {
				throw new \Exception('Dog id does not exist');
			}
		}
		
		return $id;
	}
	
	public function deleteDog($id)
	{
		$this->tableGateway->delete(array('id' => (int) $id));
	}
}
