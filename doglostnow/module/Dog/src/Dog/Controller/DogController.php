<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Dog\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Dog\Model\Dog;
use User\Model\UserDog;
use Dog\Model\DogLost;
use Dog\Form\DogForm;
use Dog\Form\LostForm;
use Address\Form\AddressForm;

class DogController extends AbstractActionController
{
    protected $dogTable;
    protected $dogLostTable;
    protected $userDogTable;
	
	public function homeAction()
    {
        return new ViewModel(array(
			//'dogs'	=> $this->getDogTable()->fetchAll(),
		));
    }
	
	public function indexAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/admin');
        
        return new ViewModel(array(
			'dogs'	=> $this->getDogTable()->fetchAll(),
		));
    }
	
    public function existingAction()
    {
//         return new ViewModel(array(
// 			'dogs'	=> $this->getDogTable()->fetchAll(),
// 		));
//        $this->userDogTable = $this->getUserDogTable();
//        var_dump($this->userDogTable->getUserDogs(1));
		$arr_data = $this->getDogTable()->getDogsByUserId(1);
		
//		$arr_data = $this->userDogTable->getUserDogs(1);
        $json_data = new JsonModel($arr_data);
        return $json_data;
    }
	
	public function addAction()
    {
		$form = new DogForm();
		$form->get('submit')->setValue('Add');
		
		$dog_id = 0;
        
		$request = $this->getRequest();
		if ($request->isPost() || $request->isXmlHttpRequest()) {
            $post_request = $request->getPost();
		    $dog = new Dog();
		    $form->setInputFilter($dog->getInputFilter());
		    if (!$request->isXmlHttpRequest()) {
                $form->setData($post_request);
		    } else {
		        $form->setData($post_request['dog_lost']['dog']);
		    }
			
			if ($form->isValid()) {
				$dog->exchangeArray($form->getData());
				$dog_id = $this->getDogTable()->saveDog($dog);
				
				if (!$request->isXmlHttpRequest()) {
    				// Redirect to list of dogs
    				return $this->redirect()->toUrl('dog/lost');
				}
			}
		}
				
        if ($request->isXmlHttpRequest()) {
            $json_data = new JsonModel(array('dog_id' => $dog_id));
            return $json_data;
        } else {
            return array('form' => $form);
        }
    }
	
	public function lostAction()
    {
		$form = new LostForm();
		$request = $this->getRequest();
		
		if ($request->isPost()) {
		    $post_request = $request->getPost();
		    $dogLost = new DogLost();
			$form->setInputFilter($dogLost->getInputFilter());
// 			error_log(print_r($post_request['dog_lost'], true));
			$form->setData($post_request['dog_lost']);

			if ($form->isValid()) {
				$dogLost->exchangeArray($form->getData());
// 				$dogLost->exchangeArray($post_request['dog_lost']);
				$this->getDogLostTable()->saveDogLost($dogLost);

				// Redirect to list of dogs
				return $this->redirect()->toUrl('/user');
			} else {
				var_dump($_POST);
				var_dump($form->getMessages());
			    error_log('Form not valid.');
			}
		 }
		 
        if ($request->isXmlHttpRequest()) {
            $json_data = new JsonModel(array('success' => 1));
            return $json_data;
        } else {
            return array('form' => $form);
        }
    }
	
	public function lostListAction()
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/admin');
        
        return new ViewModel(array(
			'dogs_lost'	=> $this->getDogLostTable()->fetchAll(),
		));
    }
	
	public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('dog', array(
				'action'	=> 'add'
			));
		}
		
		try {
			$dog = $this->getDogTable()->getDog($id);
		} catch (Exception $ex) {
			return $this->redirect()->toRoute('dog', array(
				'action'	=> 'index'
			));
		}
		
		$form = new DogForm();
		$form->bind($dog);
		$form->get('submit')->setAttribute('value', 'Edit');

		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter($dog->getInputFilter());
			$form->setData($request->getPost());

			if ($form->isValid()) {
//				$dog->exchangeArray($form->getData());
				$this->getDogTable()->saveDog($dog);

				// Redirect to list of dogs
				return $this->redirect()->toRoute('dog');
			}
		 }
		 return array(
			'id'	=> $id,
			'form' => $form
		);
    }
	
	public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('dog');
		}
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$del = $request->getPost('del', 'No');
			
			if ($del == 'Yes') {
				$id = (int) $request->getPost('id');
				$this->getDogTable()->deleteDog($id);
			}
			
			return $this->redirect()->toRoute('dog');
		}
		
		return array(
			'id'	=> $id,
			'dog' => $this->getDogTable()->getDog($id)
		);
    }
	
	public function getDogTable()
	{
		if (!$this->dogTable) {
			$sm = $this->getServiceLocator();
			$this->dogTable = $sm->get('Dog\Model\DogTable');
		}
		return $this->dogTable;
	}
	
	public function getDogLostTable()
	{
		if (!$this->dogLostTable) {
			$sm = $this->getServiceLocator();
			$this->dogLostTable = $sm->get('Dog\Model\DogLostTable');
		}
		return $this->dogLostTable;
	}
}
