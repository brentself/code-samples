<?php
namespace Dog\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

/**
 * Description of DogForm
 *
 * @author brent
 */
class DogForm extends Form
{
	public function __construct()
	{
		// we want to ignore the name passed
		parent::__construct('dog');

		$this->setAttribute('method', 'post')
             ->setHydrator(new ClassMethodsHydrator(false))
             ->setInputFilter(new InputFilter());
		
		$this->add(array(
			'type' => 'Dog\Form\DogFieldset',
			'options'	=> array(
				'use_as_base_fieldset' => true
			)
		));
		$this->add(array(
			'name' => 'submit',
			'type' => 'Submit',
			'attributes' => array(
				'value' => 'Add Dog',
				'id' => 'submitbutton',
			),
		));
	}
}
