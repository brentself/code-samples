<?php
namespace Dog\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

/**
 * Description of DogForm
 *
 * @author brent
 */
class DogFieldset extends Fieldset implements InputFilterProviderInterface
{
	public function __construct()
	{
		parent::__construct('dog');
		
		$this->add(array(
			'name' => 'name',
			'type' => 'Text',
			'options' => array(
				'label' => 'Dog Name',
			),
		));
		$this->add(array(
			'name' => 'description',
			'type' => 'Text',
			'options' => array(
				'label' => 'Dog\'s Description',
			),
		));
	}
	
	/**
	 * @return array
	 \*/
	public function getInputFilterSpecification()
	{
		return array(
			'id' => array(
				'required' => false,
			)
		);
	}
}
