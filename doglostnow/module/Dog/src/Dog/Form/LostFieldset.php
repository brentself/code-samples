<?php
namespace Dog\Form;

use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

/**
 * Description of DogForm
 *
 * @author brent
 */
class LostFieldset extends Fieldset implements InputFilterProviderInterface
{
	public function __construct()
	{
		// we want to ignore the name passed
		parent::__construct('dog_lost');

		$this->add(array(
				'name' => 'description',
				'type' => 'Text',
				'options' => array(
						'label' => 'Lost Event Description (optional)',
				),
				'attributes' => array(
						'placeholder' => 'Lost while on a walk.',
				)
		));

		$this->add(array(
				'type' => 'hidden',
				'name' => 'post_form',
				'attributes' => array(
						'id' => 'post_form',
						'value' => false
				)
		));

		$this->add(array(
				'type' => 'hidden',
				'name' => 'lost_at',
				'attributes' => array(
						'id' => 'lost_at',
				)
		));

		$months_in_year = array();
		for ($i = 1; $i <= 12; $i++) {
			$num = str_pad($i, 2, '0', STR_PAD_LEFT);
			$monthName = date("F", mktime(0, 0, 0, $i, 10));
			$months_in_year[$num] = $monthName;
		}
		$this->add(array(
			'type' => 'Select',
			'name' => 'lost_month',
			'attributes' => array(
					'value'        => date('m'),
					'id' => 'lost_month',
					'class'    => 'lost_date_selector'
			),
			'options' => array(
				'empty_option' => 'Month',
				'disable_inarray_validator' => true,
				'value_options' => $months_in_year
			)
		));

		$days_in_month = array();
		for ($i = 1; $i <= 31; $i++) {
			$num = str_pad((string)$i, 2, '0', STR_PAD_LEFT);
			$days_in_month[$num] = $i;
		}
		$this->add(array(
			'type' => 'Select',
			'name' => 'lost_day',
			'attributes' => array(
					'value'        => date('d'),
					'id' => 'lost_day',
					'class'    => 'lost_date_selector'
			),
			'options' => array(
				'empty_option' => 'Day',
				'disable_inarray_validator' => true,
				'value_options' => $days_in_month
			)
		));

		$years_in_decade = array();
		$start_year = ((int)date('Y') - 4);
		for ($i = $start_year; $i <= ($start_year + 10); $i++) {
			$years_in_decade[$i] = $i;
		}
		$this->add(array(
			'type' => 'Select',
			'name' => 'lost_year',
			'attributes' => array(
					'value'    => date('Y'),
					'id' => 'lost_year',
					'class'    => 'lost_date_selector'
			),
			'options' => array(
				'empty_option' => 'Year',
				'disable_inarray_validator' => true,
				'value_options' => $years_in_decade
			)
		));

		$this->add(array(
			'type' => 'hidden',
			'name' => 'lost_time_title',
			'options' => array(
				'label' => 'Approximate Time Lost',
				'disable_inarray_validator' => true,
			)
		));

		$hours_in_day = array();
		for ($i = 1; $i <= 12; $i++) {
			$num = str_pad((string)$i, 2, '0', STR_PAD_LEFT);
			$hours_in_day[$num] = $i;
		}
		$this->add(array(
			'type' => 'Select',
			'name' => 'lost_hour',
			'attributes' => array(
				'value' => date('h'),
				'id' => 'lost_hour',
				'class'    => 'lost_date_selector'
			),
			'options' => array(
				'empty_option' => 'Hour',
				'disable_inarray_validator' => true,
				'value_options' => $hours_in_day
			)
		));

		$min_in_hour = array();
		for ($i = 0; $i < 60; $i+=15) {
			$num = str_pad((string)$i, 2, '0', STR_PAD_LEFT);
			$min_in_hour[$num] = ': ' . $num;
		}

		$current_minute = date('i');
		$rounded_minute = 0;
		if ($current_minute >= 8 && $current_minute <= 22) {
			$rounded_minute = 15;
		} elseif ($current_minute >= 23 && $current_minute <= 37) {
			$rounded_minute = 30;
		} elseif ($current_minute >= 38 && $current_minute <= 52) {
			$rounded_minute = 45;
		}

		$this->add(array(
			'type' => 'Select',
			'name' => 'lost_minute',
			'attributes' => array(
				'value' => $rounded_minute,
				'id' => 'lost_minute',
				'class'    => 'lost_date_selector'
			),
			'options' => array(
				'empty_option' => 'Min',
				'disable_inarray_validator' => true,
				'value_options' => $min_in_hour
			)
		));

		$this->add(array(
			'type' => 'Select',
			'name' => 'lost_meridiem',
			'attributes' => array(
				'value' => date('a'),
				'id' => 'lost_meridiem',
				'class'    => 'lost_date_selector'
			),
			'options' => array(
				'empty_option' => 'AM/PM',
				'disable_inarray_validator' => true,
				'value_options' => array(
					'am' => 'AM',
					'pm' => 'PM'
				)
			)
		));

		$this->add(array(
			'type' => 'Select',
			'name' => 'dog_source',
			'attributes' => array(
				'id' => 'dog_source',
			),
			'options' => array(
				'empty_option' => 'Select Dog',
				'value_options' => array(
					'choose' => 'Choose existing dog',
					'add' => 'Add dog',
				),
				'disable_inarray_validator' => true,
			)
		));

		$this->add(array(
			'name'		=> 'dog',
			'type'		=> 'Dog\Form\DogFieldset',
			'options'	=> array(
				'label' => 'Dog Info'
			)
		));

		$this->add(array(
			'type' => 'Select',
			'name' => 'existing_dogs',
			'attributes' => array(
				'id' => 'existing_dogs'
			),
			'options' => array(
				'empty_option' => 'Existing Dogs',
				'value_options' => array(),
				'disable_inarray_validator' => true,
			)
		));

		$this->add(array(
				'name' => 'dog_id',
				'type' => 'Hidden',
				'attributes' => array(
					'id' => 'dog_id',
				),
		));

		$this->add(array(
				'type' => 'Select',
				'name' => 'address_source',
				'attributes' => array(
					'id' => 'address_source'
				),
				'options' => array(
					'empty_option' => 'Choose Address Source',
					'disable_inarray_validator' => true,
					'value_options' => array(
						'get_location' => 'Get Current Location',
						'choose' => 'Choose Existing Address',
						'enter_address' => 'Enter Address',
					)
				)
		));

		$this->add(array(
			'name' => 'address',
			'type' => 'Address\Form\AddressFieldset',
			'options'	=> array(
				'label' => 'Location/Address Info'
			)
		));

		$this->add(array(
			'name' => 'address_id',
			'type' => 'Hidden',
			'attributes' => array(
				'id' => 'address_id',
			),
		));

		$this->add(array(
			'name' => 'lost_status',
			'type' => 'Hidden',
			'attributes' => array(
				'id' => 'lost_status',
				'value' => 1,
			),
		));

		$this->add(array(
			'type' => 'Select',
			'name' => 'existing_addresses',
			'attributes' => array(
				'id' => 'existing_addresses'
			),
			'options' => array(
				'empty_option' => 'Existing Addresses',
				'value_options' => array(),
				'disable_inarray_validator' => true,
			)
		));
	}

	/**
	 * @return array
	 \*/
	public function getInputFilterSpecification()
	{
		return array(
			'dog_source' => array(
					'required' => false,
			),
			'existing_dogs' => array(
				'required' => false,
			),
			'address_source' => array(
					'required' => false,
			),
			'existing_addresses' => array(
				'required' => false,
			),
			'lost_month' => array(
				'required' => false,
			),
			'lost_day' => array(
				'required' => false,
			),
			'lost_year' => array(
				'required' => false,
			),
			'lost_hour' => array(
				'required' => false,
			),
			'lost_minute' => array(
				'required' => false,
			),
			'lost_meridiem' => array(
				'required' => false,
			),
			'description' => array(
				'required' => false,
			),
			'address_id' => array(
				'required' => false,
			),
			'lost_status' => array(
				'required' => false,
			)
		);
	}
}
