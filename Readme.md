Code Samples by Brent Self
==========================

These code samples are excerpts taken from current and past projects.

Contents
--------

###[DogLostNow](https://bitbucket.org/brentself/code-samples/src/58fca6c04f9654a84b61d69dbcea370b0478c93b/doglostnow/module/Dog/?at=master)
*PHP, SCSS, JavaScript*

Zend Framework, Foundation Framework

###[EHAT](https://bitbucket.org/brentself/code-samples/src/e9dd521074a572bbbfc64eadce06d17cc59a39e9/ehat/?at=master)
*PHP, SCSS, JavaScript*

Yii Framework, ember, Ionic, AWS Rekognition, Sketch

###[FridgeAndPantry.com](https://bitbucket.org/brentself/code-samples/src/e9dd521074a572bbbfc64eadce06d17cc59a39e9/fridgeandpantry.com/?at=master)
*PHP, SCSS, JavaScript, XD*

Custom MVC, SCSS, ES6 with Babel - [FridgeAndPantry.com Site](https://www.fridgeandpantry.com)

###[Motion Detected](https://bitbucket.org/brentself/code-samples/src/e9dd521074a572bbbfc64eadce06d17cc59a39e9/motion_detected/?at=master)
*Python*

Script executed when motion is detected by USB camera, developed for RaspberryPi and BeagleBone Black *(in progress...)*

###[Move Motors and Display Sensor Data from a Web Interface](https://bitbucket.org/brentself/robot_arm_wood/src/develop/)
*Python Flask, Redis, SQLite*

Buttons in the web interface move motors connected to the local GPIO pins (Raspberry Pi, BeagleBone, etc.) in 2 directions. Sensors on the local network or local device post JSON data to the web interface for display. A script run from the command line can be used as sensor simulators.

###[Sound Audio](https://github.com/dabbmedia/sound-audio)
*C++*

Audio app using the Qt framework *(in progress...)*

###[Vagrant](https://bitbucket.org/brentself/code-samples/src/e9dd521074a572bbbfc64eadce06d17cc59a39e9/vagrant/?at=master)
*Bash*

CentOS LAMP setup
