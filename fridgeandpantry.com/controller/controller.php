<?php
class Controller {
    public $config;
    public $view;
    public $current_page;
    public $post;
    public $get;
    public $request;
    public $files;
    public $sanitizeGet;
    public $user;

    //Constructor
    public function __construct($config) {
        @set_exception_handler(array($this, 'exception_handler'));

        $this->config = $config;
        $this->request = filter_var_array($_REQUEST, FILTER_SANITIZE_STRING);
        $this->current_page = !empty($this->request['page']) ? $this->request['page'] : '';
        
        $this->view = new StdClass;
        $this->view->template_path = $config['template_path'];
        $this->view->template_url = $config['template_url'];
        $this->view->bread_cr_sep = $config['bread_cr_sep'];
        $this->view->adsense_banner_code = $config['adsense_banner_code'];
        $this->view->sub_nav = '';

        if (get_parent_class($this) !== false) {
            $this->view->page = ucfirst(strtolower($this->current_page));
        }
        
        $this->user = new User($config);
        $this->view->isLoggedIn = $this->user->isLoggedIn();
    }

    public function exception_handler($exception) {
        $wexception = new Wexception($this->config, 'Wexception', 0, $exception);
    }

    public function displayPage() {
        if (!empty($this->current_page) && array_key_exists($this->current_page, $this->config['controllers'])) {
            return new $this->config['controllers'][$this->current_page]($this->config);
        } elseif (!array_key_exists('page', $_GET)) {
            return new $this->config['controllers']['default']($this->config);
        } else {
            $this->config['error_code'] = 404;
            return new ControllerError($this->config);
        }
    }
}
?>
