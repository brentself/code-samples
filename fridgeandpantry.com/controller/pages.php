<?php
class ControllerPages extends Controller {
	public $sanitizePost;

	public function __construct($config){
		parent::__construct($config);

		switch ($this->current_page) {
			case 'contact':
				$this->contact();
				break;
			
			case 'about':
				$this->about();
				break;

			case 'privacy':
				$this->privacy();
				break;
			
			case 'terms':
				$this->terms();
				break;

			default:
				$this->contact();
				break;
		}
	}

	private function about() {
		$this->view->title = "About FridgeAndPantry.com";
		$this->view->keywords = $this->view->title;
		$this->view->description = $this->view->title;
		
		$this->view->sub_nav = 'About Us';

		$view = $this->view;

        require_once $this->config['template_path'] . "pages/about.php";
	}

	private function privacy() {
		$this->view->title = "FridgeAndPantry.com Privacy Policy";
		$this->view->keywords = $this->view->title;
		$this->view->description = $this->view->title;
		
		$this->view->sub_nav = 'Privacy Policy';

		$view = $this->view;

        require_once $this->config['template_path'] . "pages/privacy.php";
	}

	private function terms() {
		$this->view->title = "FridgeAndPantry.com Terms of Use";
		$this->view->keywords = $this->view->title;
		$this->view->description = $this->view->title;
		
		$this->view->sub_nav = 'Terms of Use';

		$view = $this->view;

        require_once $this->config['template_path'] . "pages/terms.php";
	}
}
?>