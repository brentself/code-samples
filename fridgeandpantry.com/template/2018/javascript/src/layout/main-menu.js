class MainMenu {
	constructor() {
	    let div_menu = document.getElementById('nav'),
	    div_menu_btn = document.getElementById('main-menu-icon'),
	    div_container = document.getElementById('container'),
	    body = document.getElementById('body');

	    div_menu_btn.addEventListener("click", function(event) {
			div_menu.style.display = 'block';
		});

		div_menu.addEventListener("click", function(event) {
			div_menu.style.display = 'none';
		});

		document.addEventListener('keyup', function(e) {
			if (e.key == 'Escape') {
				document.getElementById('nav').style.display = 'none';
			}
		});
	}
}
