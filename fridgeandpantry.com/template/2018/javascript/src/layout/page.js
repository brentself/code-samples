class Page extends MainMenu {
	constructor() {
		super();
	}

	toggleElement(element_id) {
	    var elementStyle = document.getElementById(element_id).style;
	    if (elementStyle) {
	        elementStyle.display = (elementStyle.display == 'none') ? 'block' : 'none';
	    }
	}
}

document.addEventListener("DOMContentLoaded", function(event) {
	var page = new Page();
});
