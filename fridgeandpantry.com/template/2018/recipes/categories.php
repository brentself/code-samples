<?php require_once $view->template_path . 'header.php'; ?>

<div id="categories" class="box_content_nobg">
	<h1 class="content_title"><?php echo $view->sub_nav; ?></h1>

    <div id="ad_top"><?php echo $view->adsense_banner_code; ?></div>
	
    <ul id="categories_container">
	<?php foreach ($view->categories as $category) { ?>
        <li id="cat_<?php echo $category->cat_name; ?>" class="box_item category">
            <div class="catImg">
            	<a href="/recipes/<?php echo $category->cat_name; ?>">
            	   <img class="thumbs" src="<?php echo $view->template_url; ?>images/categories/thumbs/<?php echo $category->cat_image; ?>" alt="<?php echo $category->cat_name; ?>" />
            	</a>
            </div>
            <div class="catTitle">
                <a href="/recipes/<?php echo $category->cat_name; ?>"><?php echo $category->cat_name; ?></a>
            </div>
        </li>
    <?php } ?>
	</ul>

    <div id="ad_bottom"><?php echo $view->adsense_banner_code; ?></div>
</div>

<?php require_once $view->template_path . 'footer.php'; ?>