<?php
ini_set('display_errors', E_ERROR | E_PARSE);

setlocale(LC_ALL, 'en_US');
ini_set('date.timezone', 'America/Denver');

$sesh_id = session_id();
if( empty( $sesh_id ) ) {
    session_start();
}

require_once './config/config.php';

spl_autoload_register(function ($class_name) {
    if (strpos($class_name, 'Controller') !== false) {
        $controllerNameAndPath = __DIR__ . '/controller/' . strtolower(str_replace('Controller', '', $class_name)) . '.php';
        
        if (file_exists($controllerNameAndPath)) {
        	require_once $controllerNameAndPath;
        }
    } else {
        $moduleNameAndPath = __DIR__ . '/module/' . strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class_name)) . '/' . strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class_name)) . '.php';

        if (file_exists($moduleNameAndPath)) {
	        require_once $moduleNameAndPath;
	    }
    }
});

require_once $config['site_path'] . 'controller/controller.php';

$controller = new Controller($config);
$controller->displayPage();
?>