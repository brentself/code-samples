<?php
/*
* Created on Jun 20, 2008
*
* by Brent Self
*/
class Recipes extends Module {
    private $navCats;
    private $catId;
    private $recId;
    private $subCount = 0;

    public function __construct($config) {
        parent::__construct($config);
    }

    //GetRecipe
    function getRecipes($data = array(), $withIngredients = false) {
        $recipe = array();

        $sql_select = "SELECT DISTINCT 
            r.rec_id, 
            r.rec_name, 
            r.description, 
            r.rec_url, 
            r.rec_instructions, 
            r.rec_servings, 
            r.rec_by, 
            r.rec_image1, 
            r.date_published, 
            r.cat_id, 
            r.cook_time, 
            r.cooking_method, 
            r.prep_time, 
            r.recipe_cuisine, 
            r.total_time, 
            c.cat_name, 
            c.cat_info, 
            u.username ";

        $sql_from = "FROM recipes r 
            LEFT JOIN recipes_categories c
                ON r.cat_id=c.cat_id 
            LEFT JOIN users u
                ON r.user_id=u.user_id ";

        $sql_where = '';

        if (!empty($data)) {
            $sql_where .= "WHERE ";

            $keys = array_keys($data);
            for ($i = 0; $i < count($keys); $i++) {
                $key = $keys[$i];

                switch ($key) {
                    case 'cat_name':
                        $sql_where .= "c.";
                        break;
                    
                    case 'cat_info':
                        $sql_where .= "c.";
                        break;
                    
                    case 'username':
                        $sql_where .= "u.";
                        break;
                    
                    default:
                        $sql_where .= "r.";
                        break;
                }

                $sql_where .= $key . " = :" . $key . " " . (($i >= 0 && $i < count($keys) - 1) ? "OR " : "");
            }
        }
        
        $sql = $sql_select;
        $sql .= $sql_from;
        $sql .= $sql_where;
        $sql .= "ORDER BY r.rec_name ASC ";
        
        $sql = str_replace(array("\n", "\r", "\t", "  "), " ", $sql);

        $resultset = $this->db->query($sql, $data);

        if (empty($resultset)) {
            error_log('Failed to issue query for recipes, error message : ' . $sql);
            return false;
        }

        $recipes = array();

        foreach ($resultset as $row) {
            $recipe['rec_id'] = $row['rec_id'];
            $recipe['rec_name'] = html_entity_decode(html_entity_decode(stripslashes($row['rec_name'])));
            $recipe['description'] = html_entity_decode(html_entity_decode(stripslashes($row['description'])));
            $recipe['rec_url'] = $row['rec_url'];

            $recipe['rec_instructions'] = $this->correctHtmlStringEncoding($row['rec_instructions']);
            
            $recipe['rec_servings'] = stripslashes($row['rec_servings']);
            $recipe['rec_by'] = html_entity_decode(html_entity_decode(stripslashes($row['rec_by'])));
            $recipe['rec_image1'] = stripslashes($row['rec_image1']);
            $recipe['date_published'] = $row['date_published'];
            $recipe['username'] = $row['username'];
            $recipe['cook_time'] = stripslashes($row['cook_time']);
            $recipe['cooking_method'] = stripslashes($row['cooking_method']);
            $recipe['prep_time'] = stripslashes($row['prep_time']);
            $recipe['recipe_cuisine'] = stripslashes($row['recipe_cuisine']);
            $recipe['total_time'] = stripslashes($row['total_time']);
            $recipe['cat_id'] = $row['cat_id'];
            $recipe['cat_name'] = stripslashes($row['cat_name']);
            $recipe['cat_info'] = html_entity_decode(html_entity_decode(stripslashes($row['cat_info'])));

            if ($withIngredients === true) {
                $recipe['ingredients'] = $this->getRecipeIngredients(array('rec_id' => $recipe['rec_id']));
            }

            $recipes[] = $recipe;
        }

        return $recipes;
    }

    public function correctHtmlStringEncoding($htmlString) {
        $cleanString = '';

        $instrTextEnc = mb_detect_encoding($htmlString);

        if ($instrTextEnc !== 'UTF-8' && $instrTextEnc !== false) {
            $cleanString = mb_convert_encoding($htmlString, 'UTF-8', $instrTextEnc);
        } else {
            $cleanString = $htmlString;
        }

        $cleanString = html_entity_decode(html_entity_decode(stripslashes($cleanString)));

        return $cleanString;
    }

    //Update recipe
    public function updateRecipe($data) {
        $user = unserialize($_SESSION['user']);
        $user_id = $user['user_id'];
        
        $sql = str_replace(array("\r", "\n", "\t"), "", "UPDATE recipes
            SET
                rec_name = :rec_name,
                description = :description,
                prep_time = :prep_time,
                cook_time = :cook_time,
                total_time = :total_time,
                rec_url = :rec_url,
                rec_instructions = :rec_instructions,
                rec_servings = :rec_servings,
                cat_id = :cat_id,
                user_id = :user_id,
                rec_by = :rec_by,
                rec_image1 = :rec_image1,
                rec_complete = :rec_complete
            WHERE rec_id = :rec_id");

        $resultset = $this->db->query($sql, $data);

        if ($resultset === false) {
            return 0;
        } else {
            return 1;
        }
    }

    //AddRecipeIngredient
    function addRecipeIngredient($data) {
        $user = unserialize($_SESSION['user']);
        $user_id = $user['user_id'];

        $cleanData = array(
            'rec_id' => $data['rec_id'],
            'recing_name' => addslashes(strip_tags($data['recing_name'])),
            'qty' => addslashes(strip_tags($data['qty'])),
            'un_id' => $data['un_id'],
            'ndb_no' => (isset($data['ndb_no']) && !empty($data['ndb_no'])) ? $data['ndb_no']: ""
        );

        if (!empty($cleanData['rec_id']) && $cleanData['rec_id'] !== 0 && $cleanData['rec_id'] !== "" && $cleanData['rec_id'] !== "0") {
            $sql = "INSERT recipes_ingredients (
                        rec_id, 
                        recing_name, 
                        recing_qty, 
                        un_id, 
                        ndb_no
                    ) VALUES (
                        ':rec_id',
                        ':recing_name',
                        ':qty',
                        ':un_id',
                        ':ndb_no'
                    )";

            $resultset = $this->db->query($sql, $cleanData);

            if ($resultset === false) {
                return 0;
            } else {
                $id = $this->db->insertId;
                return $id;
            }
        } else {
            return 0;
        }
    }

    //GetNutSuggestions
    function GetNutSuggestions($obj) {
        $srch = $obj['terms'];
        $nut = new Nutrition();
        $suggestions = $nut->GetSuggestions($srch);

        return $suggestions;
    }

    //GetFoods
    function getFoods() {
        $nut = new Nutrition($this->config);
        $foodGrps = $nut->GetFoodGroups();
        $foods = array();
        foreach($foodGrps as $obj) {
            $foodsInGroups = $nut->GetFoodsInGroup($obj->id);
            foreach($foodsInGroups as $obj2){
                $foods[] = $obj2;
            }
        }

        return $foods;
    }

    //GetRecipeNutrition
    function getRecipeNutrition($ingredients) {
        require_once "Zend/Measure/Cooking/Weight.php";
        require_once "Zend/Locale.php";

        $nut = new Nutrition($this->config);

        $info = array();

        foreach ($ingredients as $obj) {
            if($obj->ndb_no != "" && $obj->ndb_no != NULL && !empty($obj->units)) {
                $ingGrams = 0;
                $ingQty = 0;
                $wholeNum = 0;
                $ingQty = $obj->qty;
                if (strstr(trim($obj->qty), ' ') !== FALSE) {
                    $numsInString = explode(' ', trim($obj->qty));
                    $wholeNum = (int)$numsInString[0];
                }
                if (strstr($obj->qty, '/') !== FALSE) {
                    $fracts = explode('/', $obj->qty);
                    $ingQty = (($wholeNum * (int)$fracts[1]) + (int)$fracts[0]) / (int)$fracts[1];
                }
                try {
                    // convert it to the qty/units of the nutrition info grams
                    $locale = new Zend_Locale('en_US');
                    $unit = new Zend_Measure_Cooking_Weight($ingQty,
                                                    strtoupper(rtrim($obj->units_full, 's')),
                                                    $locale);

                    $ingGrams = $unit->convertTo(Zend_Measure_Cooking_Weight::GRAM);
                } catch (Exception $e) {
                    error_log('Conversion failed for: ' .  $obj->qty . ' ' . $obj->units_full);
                    error_log($e);
                }

                $nuts = $nut->GetFood($obj->ndb_no);

                foreach ($nuts as $ingNutInfo) {
                    if ($ingNutInfo->nutr_val !== 0 && $ingNutInfo->nutr_val !== "0" && strstr($ingNutInfo->nutrdesc,':') === FALSE) {
                        $nutName = $ingNutInfo->nutrdesc;
                        $nutValue = $ingNutInfo->nutr_val;

                        if (array_key_exists($nutName, $info) === TRUE && $nutValue > 0) {
                            $info[$nutName][0] += round(((int)$nutValue/100) * (int)$ingGrams);
                        } else {
                            $info[$nutName][0] = 0;
                        }

                        $info[$nutName][1] = $ingNutInfo->units;
                    }
                }
            }
        }

        return $info;
    }

}
?>
